import '../exceptions/exceptions.dart';
import '../models/models.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

abstract class AuthenticationService {
  Future<User> getCurrentUser();

  Future<User> signInWithEmailAndPassword(String email, String password);

  Future<void> signOut();
}

class FakeAuthenticationService extends AuthenticationService {
  @override
  Future<User> getCurrentUser() async {
    return null; // return null for now
  }

  @override
  Future<User> signInWithEmailAndPassword(String email, String password) async {
    print('Inside signInWithEmailAndPassword with email:$email and password:$password');
    await Future.delayed(Duration(seconds: 1)); // simulate a network delay

    //if (email.toLowerCase() != 'test@domain.com' || password != 'testpass123') {
    /*if (email.toLowerCase() != 'abcd' || password != 'abcd') {
      throw AuthenticationException(message: 'Wrong username or password');
    }
    return User(name: 'Test User', email: email);*/

    try {
      String url = 'https://reqres.in/api/login';
      //var data = {'email': 'eve.holt@reqres.in', 'password': 'cityslicka'};
      var data = {'email': email, 'password': password};
      final result = await http.Client().post(url, headers: _setHeaders(), body: jsonEncode(data));
      print('result.statusCode: ${result.statusCode}');
      if (result.statusCode == 200) {
        print('successfully logged in');
        return User(name: email.substring(0,email.indexOf('@')).toUpperCase(), email: email);
      } else
        throw AuthenticationException(message: 'Wrong username or password');
    } catch (e) {}
  }

  @override
  Future<void> signOut() {
    return null;
  }

  _setHeaders() => {'Content-Type': 'application/json', 'Accept': 'application/json'};
}

part of 'signup_bloc.dart';

abstract class SignUpState extends Equatable {
  const SignUpState();
}

class SignUpInitial extends SignUpState {
  const SignUpInitial();
  @override
  List<Object> get props => [];
}

class SignUpSubmitting extends SignUpState{
  const SignUpSubmitting();

  @override
  List<Object> get props => [];
}

class SignUpSubmitted extends SignUpState{
  final bool isSubmitted;
  const SignUpSubmitted(this.isSubmitted);

  @override
  List<Object> get props => [];
}

class SignUpError extends SignUpState{
  final String message;
  const SignUpError(this.message);

  @override
  List<Object> get props => [];
}
part of 'signup_bloc.dart';

abstract class SignUpEvent extends Equatable {
  const SignUpEvent();
}

class CreateUserEvent extends SignUpEvent {
  final String userName;
  final String userJob;

  CreateUserEvent(this.userName, this.userJob);

  @override
  List<Object> get props => [];
}

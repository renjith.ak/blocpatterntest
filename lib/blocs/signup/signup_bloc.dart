import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc_authentication/repository/signup_repository.dart';

part 'signup_event.dart';

part 'signup_state.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  final SampleSignUpRepository _sampleSignUpRepository;

  SignUpBloc(this._sampleSignUpRepository) : super(SignUpInitial());

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    if (event is CreateUserEvent) {
      try {
        yield SignUpSubmitting();

        final bool isSubmitted =
            await _sampleSignUpRepository.submitUserDetails(event.userName, event.userJob);

        yield SignUpSubmitted(isSubmitted??false); //if null then false
      } catch (e) {
        yield SignUpError(e.toString());
      }
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_authentication/blocs/signup/signup_bloc.dart';
import 'package:flutter_bloc_authentication/repository/signup_repository.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final _userNameController = TextEditingController();
  final _userJobController = TextEditingController();
  final SignUpBloc _signupBloc = SignUpBloc(SampleSignUpRepository());

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _userNameController.dispose();
    _userJobController.dispose();
    _signupBloc.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Sign Up')),
        body: BlocConsumer<SignUpBloc, SignUpState>(
          listener: (context, state) {
            if (state is SignUpError) {
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text(state.message),
              ));
            }
          },
          cubit: _signupBloc,
          builder: (context, state) {
            if (state is SignUpInitial) {
              return initialInputFields();
            } else if (state is SignUpSubmitting) {
              return signUpSubmitInProgress();
            } else if (state is SignUpSubmitted) {
              return buildColumnWithData(state.isSubmitted);
            } else
              return initialInputFields();
          },
        ));
  }

  Widget signUpSubmitInProgress() {
    return Center(child: CircularProgressIndicator());
  }

  Widget initialInputFields() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextFormField(
          decoration: InputDecoration(
            labelText: 'User Name',
            filled: true,
            isDense: true,
          ),
          controller: _userNameController,
          autocorrect: false,
          validator: (value) {
            if (value == null) {
              return 'User Name is required.';
            }
            return null;
          },
        ),
        SizedBox(height: 12),
        TextFormField(
          decoration: InputDecoration(
            labelText: 'User Job',
            filled: true,
            isDense: true,
          ),
          controller: _userJobController,
          autocorrect: false,
          validator: (value) {
            if (value == null) {
              return 'User Job is required.';
            }
            return null;
          },
        ),
        SizedBox(height: 20),
        RaisedButton(
            color: Theme.of(context).primaryColor,
            textColor: Colors.white,
            padding: const EdgeInsets.all(16),
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(8.0)),
            child: Text('SUBMIT'),
            onPressed: () {
              _submitUserDetails(_userNameController.text, _userJobController.text);
            }),
      ],
    );
  }

  Column buildColumnWithData(bool isSubmitted) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        isSubmitted
            ? Text(
                'User registered successfully.',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                ),
              )
            : Text(
                'Failed to Register the User.',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                ),
              ),
        initialInputFields(),
      ],
    );
  }

  void _submitUserDetails(String userName, String userJob) {

    //add event to the bloc
    _signupBloc.add(CreateUserEvent(userName, userJob));

    //clear the input fields
    _userNameController.text = '';
    _userJobController.text = '';
  }
}

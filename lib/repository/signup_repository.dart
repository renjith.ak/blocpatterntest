import 'dart:convert';
import 'package:http/http.dart' as http;

abstract class SignUpRepository {
  Future<bool> submitUserDetails(String userName, String userJob);
}

class SampleSignUpRepository implements SignUpRepository {
  @override
  Future<bool> submitUserDetails(String userName, String userJob) {
    print('inside SubmitUserDetails');
    bool isRegistered = false;
    return Future.delayed(Duration(seconds: 1), () async{
      if(userName.isEmpty || userJob.isEmpty)
        return isRegistered;
      print('just before calling _registerUserDetails');
      isRegistered = await _registerUserDetails(userName,userJob);
      return isRegistered;
    });
  }

  Future<bool> _registerUserDetails(String userName, String userJob) async
  {
    print('inside _registerUserDetails');
    bool isRegistered = false;
    try {
      String url = 'https://reqres.in/api/users';
      var data = {'name': userName, 'job': userJob};
      final result = await http.Client().post(url, headers: _setHeaders(), body: jsonEncode(data));
      print('result.statusCode: ${result.statusCode}');
      if (result.statusCode == 201) {
        print('successfully created');
        isRegistered = true;
      } else
        isRegistered = false;
    } catch (e) {
      print('inside catch of _registerUserDetails in signup_repository:${e.toString()}');
      isRegistered = false;
    }
    return isRegistered;
  }
  _setHeaders() => {'Content-Type': 'application/json', 'Accept': 'application/json'};
}
